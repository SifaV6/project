Required 
- nodejs
- monogodb

How to run - Dev mode:
- npm install
- npm install -g node-inspector nodemon grunt-cli bower component mocha
- bower install
- component install
- install and run mongodb
- grunt server

Deploy mode:
- npm install -g forever
- forever start -c nodemon app.js

Nothing yet.

Still has a lot of validation that need to be done:
- model
- checking if user is authed
- check if user actually exists before CRUD
