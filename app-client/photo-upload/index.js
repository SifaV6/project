var Upload = require('s3');
var drop = require('drop-anywhere');
var filePicker = require('file-picker');
var request = require('superagent');
var domify = require('domify');
var photoItemHTML = require('./photo-item.html');

request.get('/signS3').end(function (err, res) {
  var el = document.querySelector('[data-drop=upload]');

  if (!el || res.status !== 200) {
    return;
  }
  S3 = res.body;

  var photoList = document.querySelector('.photo-list');
  var photos = document.querySelectorAll('.photo-list li img');
  var photoUrls = [];
  for (var i = 0; i < photos.length; i++) {
    var photoEl = photos[i];
    photoUrls.push(photoEl.src);
  }

  function uploadItem (item) {
    console.log(item);
    // check if legit photo
    if (!(/\.(gif|jpg|jpeg|tiff|png)$/i).test(item.name)) {
      alert('File must be an image of type jpg, jpeg, gif, tiff or png');
      return;
    }

    // add photo item to list
    var photoItem = domify(photoItemHTML);
    photoList.appendChild(photoItem);
    var progress = photoItem.querySelector('.progress-bar');

    // generate file name
    var fileExt = item.name.split('.').pop();
    var uid = Math.random() * 1e10 | 0;
    var name = 'property-photos/' + uid + '.' + fileExt;

    // upload!
    var upload = Upload(item, { name: name });

    upload.on('progress', function (e) {
      progress.style.width = e.percent + '%';
    });

    upload.end(function (err) {
      if (err) throw err;

      var img = photoItem.querySelector('img');
      img.src = upload.url;
      console.log('successfully uploaded ' + upload.file.name + ' to S3 at ' + upload.url);

      photoUrls.push(upload.url);
      var propertyId = el.getAttribute('data-property-id');
      request
        .put('/api/properties/' + propertyId)
        .send({ photos: photoUrls })
        .end(function (err, res) {
          if (err) throw err;

          progress.style.display = 'none';
          console.log(res);
        });
    });
  }

  drop(function (e) {
    e.items.forEach(uploadItem);
  });

  document.querySelector('.add-photo-btn')
    .addEventListener('click', function () {
      filePicker(function (files) {
        [].forEach.call(files, uploadItem);
        // for (var i = 0; i < files.length; i++) {
        //  var file = files[i];
        //  file.kind = 'file';
        //  uploadItem(file);
        // }
      });
    });
});