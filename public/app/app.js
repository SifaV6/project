var module = angular.module('buynlease', []);

/*
 * Controllers
 */

module.controller('PropertySearchCtrl', ['$scope', '$http', function ($scope, $http) {
  // form variables
  $scope.criteria = {
    propertyType: [],
    rentType: [],
    suburb: '',
    bedrooms: 'any',
    price: {
      max: ''
    }
  };

  // property list
  $scope.properties = [];

  $scope.search = function () {
    // build query string
    var queryStr = '?';
    var i;
    for (i = 0; i < $scope.criteria.propertyType.length; i++) {
      queryStr = queryStr + 'propertyType=' + $scope.criteria.propertyType[i] + '&';
    }
    for (i = 0; i < $scope.criteria.rentType.length; i++) {
      queryStr = queryStr + 'rentType=' + $scope.criteria.rentType[i] + '&';
    }
    queryStr = queryStr + 'suburb=' + $scope.criteria.suburb + '&';
    queryStr = queryStr + 'bedrooms=' + $scope.criteria.bedrooms + '&';
    queryStr = queryStr + 'price[max]=' + $scope.criteria.price.max + '&';

    // call API
    $http.get('/api/properties' + queryStr)
      .success(function (data, status, headers, config) {
        $scope.properties = data;
      });
  };
}]);

module.controller('PropertyCreateCtrl', ['$scope', '$http', function ($scope, $http) {
  $scope.property = {};
  $scope.errors = [];

  $scope.create = function () {
    // console.log($scope.property);
    $http.post('/api/properties', $scope.property)
      .success(function (data, status, headers, config) {
        console.log(data);
        window.location.href = '/properties/' + data._id + '/edit';
      })
      .error(function (data, status, headers, config) {
        $scope.errors = data.error;
      });
  };
}]);

module.controller('PropertyUpdateCtrl', ['$scope', '$http', function ($scope, $http) {
  $scope.property = {};

  $scope.property.title = $('input[name=title]').attr('value') || "";
  $scope.property.description = $('textarea[name=description]').text();
  $scope.property.bedrooms = $('select[name=bedrooms]').val() || 0;
  $scope.property.bathrooms = $('select[name=bathrooms]').val() || 0;
  $scope.property.carspaces = $('select[name=carspaces]').val() || 0;

  $scope.edit = function (id) {
    // console.log($scope.property);
    $http.put('/api/properties/' + id, $scope.property)
      .success(function (data, status, headers, config) {
        window.location.href = '/';
      })
      .error(function (data, status, headers, config) {
        console.log(data);
      });
  };
}]);


module.controller('PropertyGetCtrl', ['$scope', '$http', function ($scope, $http) {
  $scope.get = function (id) {
    $http.get('/api/properties/' + id)
      .success(function (data, status, headers, config) {
        $scope.property = data;
      });
    $('#myModal').modal();
  };
}]);

/*
 * Directives
 */

/* Checklist directives */
// Checklist label - displays contents of checklist in a label
// The ng-model passed in must be pre-declared inside the controller as a list
module.directive('checklistLabel', function () {
  return {
    scope: {
      list: '=ngModel'
    },
    template: '{{label}}',
    link: function (scope, element, attrs) {
      scope.$watch('list', function () {
        if (scope.list.length !== 0) {
          if (scope.list.length > 1) {
            scope.label = 'Multiple';
          } else {
            var first = scope.list[0];
            scope.label = first.charAt(0).toUpperCase() + first.slice(1);
          }
        } else {
          scope.label = 'Any';
        }
      }, true);
    }
  };
});

// Checklist item
module.directive('checklistItem', function () {
  return {
    scope: {
      list: '=ngModel'
    },
    link: function (scope, element, attrs) {
      var handler = function (setup) {
        var checked = element.prop('checked');
        var index = scope.list.indexOf(attrs.value);

        if (checked && index === -1) {
          if (setup) {
            element.prop('checked', false);
          } else {
            scope.list.push(attrs.value);
          }
        } else if (!checked && index !== -1) {
          if (setup) {
            element.prop('checked', true);
          } else {
            scope.list.splice(index, 1);
          }
        }
      };

      var setupHandler = handler.bind(null, true);
      var changeHandler = handler.bind(null, false);

      element.bind('change', function () {
        scope.$apply(changeHandler);
      });
      scope.$watch('list', setupHandler, true);
    }
  };
});

// Checklist clear - check this checkbox to clear all items
module.directive('checklistClear', function () {
  return {
    scope: {
      list: '=ngModel'
    },
    link: function (scope, element, attrs) {
      element.bind('change', function () {
        scope.$apply(function () {
          var checked = element.prop('checked');

          if (checked) {
            scope.list = [];
          } else {
            if (scope.list.length === 0) {
              element.prop('checked', true);
            }
          }
        });
      });
      scope.$watch('list', function () {
        if (scope.list.length !== 0) {
          element.prop('checked', false);
        } else {
          element.prop('checked', true);
        }
      }, true);
    }
  };
});

/* Button directives */
// Grouped radio buttons
// Used instead of data-toggle="buttons" attribute from bootstrap
// because bootstrap's js doesn't work with angular models
// Since the radio input is wrapped by the label, we add the active class to the element's parent (label)
module.directive('btnRadio', function () {
  var activeClass = 'active';
  var toggleEvent = 'click';

  return {
    scope: {
      model: '=ngModel'
    },
    link: function (scope, element, attrs) {
      element.bind(toggleEvent, function () {
        if (!element.parent().hasClass(activeClass)) {
          scope.$apply(function () {
            scope.model = attrs.value;
            element.parent().toggleClass(activeClass, angular.equals(scope.model, attrs.value));
          });
        }
      });
      scope.$watch('model', function () {
        element.parent().toggleClass(activeClass, angular.equals(scope.model, attrs.value));
      }, true);
    }
  };
});

module.directive('preventDefault', function () {
  return {
    link: function (scope, element, attrs) {
      element.bind('click', function (e) {
        e.preventDefault();
      });
    }
  };
});