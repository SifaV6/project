/* Helper functions for dealing with schemas */

// put string or object in an array
exports.toArray = function (methods) {
	if (typeof methods === 'string' || (typeof methods === 'object' && !Array.isArray(methods))) {
		methods = [methods];
	}	else if (Array.isArray(methods)) {
		methods = methods;
	} else {
		methods = [];
	}

	return methods;
};

// pass in a string 'blah' or object { 'blah': ['hi'] } 
// return an object of form {methodName: 'blah', args: ['hi']}
exports.getMethod = function (method) {
	var obj = { method: undefined, args: undefined };

	if (typeof method === 'string') {
		obj.method = method;
	} else if (typeof method === 'object') {
		var methodName = Object.keys(method)[0];
		obj.method = methodName;
		obj.args = method[methodName];
	}

	return obj;
};

// Get deep object from properties array - e.g.
// if price.max is 10, getDeepObject('price.max'.split('.'), obj) will be 10
exports.getDeepObject = function (properties, obj) {
	if (properties.length === 0) {
		return obj;
	}

	var property = properties[0];
	if (!obj.hasOwnProperty(property)) {
		return undefined;
	}

	return exports.getDeepObject(properties.slice(1), obj[property]);
};

// Traverse through all nodes in an object
exports.traverse = function (obj, callback, afterCallback) {
	for (var key in obj) {
		callback.apply(this, [obj, key, obj[key]]);
		if (typeof(obj[key]) === "object") {
			exports.traverse(obj[key], callback, afterCallback);
		}
		afterCallback.apply(this, [obj, key, obj[key]]);
  }
};