// Set the res.locals.currentUser if there is a req.user, i.e. there is an logged in user
// To expose the user's details to the views through currentUser

module.exports = function (req, res, next) {
	if (req.user) {
		res.locals.currentUser = req.user;
	}

	next();
};