var SALT_WORK_FACTOR = 10;

var bcrypt = require('bcrypt-nodejs');

// callback should be of form callback(err, hashedPassword)
exports.hash = function (password, callback) {
	// generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
		if (err) {
			return callback(err);
		}

		// hash the password using our new salt
		bcrypt.hash(password, salt, null, function (err, hash) {
			if (err) {
				return callback(err);
			}

			// override the cleartext password with the hashed one
			callback(null, hash);
		});
	});
};

// callback should be of form callback(err, isMatch)
exports.compare = function (candidatePassword, hashed, callback) {
	bcrypt.compare(candidatePassword, hashed, function (err, isMatch) {
		if (err) {
			return callback(err);
		}

		callback(null, isMatch);
	});
};