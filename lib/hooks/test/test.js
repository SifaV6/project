var Hooks = require('../');
var should = require('chai').should();

describe('Hooks', function () {
  describe('module.exports', function () {
    it('should return a function', function () {
      Hooks.should.be.a('function');
    });
  });

  describe('mixin', function () {
    it('should have before and after functions after mixin', function () {
      var obj = {};
      Hooks(obj);

      obj.before.should.be.a('function');
      obj.after.should.be.a('function');
    });
  });

  describe('before()', function () {
    it('should call all before hooks', function () {
      var obj = {
        beforeCalled: false
      };
      Hooks(obj);

      obj.fn = function () {
        return this.beforeCalled;
      };

      obj.before('fn', function () {
        this.beforeCalled = true;
      });
      
      obj.fn().should.equal(true);
      obj._hooks.fn.before.should.have.length(1);

      obj.before('fn', function () {
        this.beforeCalled = false;
      });

      obj.fn().should.equal(false);
      obj._hooks.fn.before.should.have.length(2);
    });
  });

  describe('after()', function () {
    it('should call all after hooks', function () {
      var obj = {
        afterCalled: false
      };
      Hooks(obj);

      obj.fn = function () {
        return this.afterCalled;
      };

      obj.after('fn', function () {
        this.afterCalled = true;
      });
      
      obj.fn().should.equal(false);
      obj._hooks.fn.after.should.have.length(1);
      obj.afterCalled.should.equal(true);

      obj.after('fn', function () {
        this.afterCalled = false;
      });

      obj.fn().should.equal(true);
      obj._hooks.fn.after.should.have.length(2);
      obj.afterCalled.should.equal(false);
    });
  });
});