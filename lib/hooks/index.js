module.exports = Hooks;

function Hooks(obj) {
	if (obj) {
		return mixin(obj);
	}
}

function mixin(obj) {
	for (var key in Hooks.prototype) {
		obj[key] = Hooks.prototype[key];
	}
}

Hooks.prototype.before = function (method, fn) {
	basicHook(this, method, fn, 'before');
};

Hooks.prototype.after = function (method, fn) {
	basicHook(this, method, fn, 'after');
};

function basicHook(obj, method, fn, hook) {
	createHooks(obj, method);

	if (fn) {
		obj._hooks[method][hook].push(fn);
	}
}

function createHooks(obj, method) {
	obj._hooks = obj._hooks || {};

	if (!obj._hooks[method]) {
		obj._hooks[method] = {};
		obj._hooks[method].before = [];
		obj._hooks[method].after = [];

		var oldFn = obj[method];
		obj[method] = wrapWithHooks(obj, method, oldFn);
	}
}

function wrapWithHooks(obj, method, fn) {
	return function () {
		var returned;

		var befores = obj._hooks[method].before;
		for (var i = 0; i < befores.length; i++) {
			befores[i].apply(obj);
		}

		returned = fn.apply(obj, Array.prototype.slice.call(arguments));

		var afters = obj._hooks[method].after;
		for (i = 0; i < afters.length; i++) {
			afters[i].apply(obj);
		}

		return returned;
	};
}