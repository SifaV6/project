var Filter = require('validator').Filter;
var mapper = require('object-mapper');
var helpers = require('./schema-helpers');

/* Overrides */

// Override default behavior of turning value to string
// if is array or object then don't convert it to str
Filter.prototype.convert = Filter.prototype.sanitize = function(str) {
		if (typeof str !== 'object' || str === null) {
			this.str = str == null ? '' : str + '';
		} else {
			this.str = str;
		}
    
    return this;
};

/* Extensions */

// Note: this.wrap is necessary for .chain to work

Filter.prototype.toString = function () {
	this.modify(this.str + '');

	return this.wrap(this.str);
};
// convert to array if it's not already an array
Filter.prototype.toArray = function (separator, limit) {
	if (!Array.isArray(this.str)) {
		this.modify(this.str.split(separator, limit));
	}
	
	return this.wrap(this.str);
};
Filter.prototype.toLowerCase = function () {
	this.modify(this.str.toLowerCase());

	return this.wrap(this.str);
};
// go through the array and apply methods
// str must be an array of course
Filter.prototype.each = function (methods) {
	if (methods === null || methods === undefined) {
		return this.wrap(this.str);
	}

	methods = helpers.toArray(methods);
	var filter = new Filter();

	for (var i = 0; i < this.str.length; i++) {
		for (var j = 0; j < methods.length; j++) {
			var method = methods[j];
			method = helpers.getMethod(method);

			if (filter[method.method]) {
				filter.sanitize(this.str[i]);
				this.str[i] = filter[method.method].apply(filter, method.args);
			}
		}
	}

	return this.wrap(this.str);
};

/* Main methods */

exports.filter = function (obj, schema) {
	var filter = new Filter();

	for (var property in schema) {
		// get parent and child elements
		var parentPath = property.split('.');
		var child = parentPath.pop();
		var parent = helpers.getDeepObject(parentPath, obj);

		// if object has this property
		if (parent && parent.hasOwnProperty(child)) {
			// turn this to array of methods to be traversed
			var methods = helpers.toArray(schema[property]);

			for (var i = 0; i < methods.length; i++) {
				var method = methods[i];
				method = helpers.getMethod(method);

				if (filter[method.method]) {
					filter.sanitize(parent[child]);
					parent[child] = filter[method.method].apply(filter, method.args);
				}
			}
		}
	}
};

exports.map = function (source, target, schema) {
	var mapped = mapper.merge(source, target, schema);
	// remove all null properties in the beforeCallback
	// remove all empty objects in the afterCallback
	helpers.traverse(mapped, function (obj, key, value) {
		// if value is null, undefined, "" or NaN but not 0 or false
		if ((value !== 0 && value !== false) && !value) {
			delete obj[key];
		}
	}, function (obj, key, value) {
		if (typeof value === 'object' && Object.keys(value).length === 0) {
			delete obj[key];
		}
	});

	return mapped;
};