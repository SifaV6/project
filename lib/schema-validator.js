var Validator = require('validator').Validator;
var helpers = require('./schema-helpers');

/* Overrides */

Validator.prototype.error = function (msg) {
	this._errors.push(msg);
	return this;
};

Validator.prototype.getErrors = function () {
	if (this._errors && this._errors.length > 0) {
		return this._errors;
	} else {
		return null;
	}
};

/* Main methods */

// nonStrict option means required properties are no longer required!
// useful for partial updates, etc.
exports.validate = function (obj, schema, options) {
	var validator = new Validator();

	for (var property in schema) {
		// get parent and child elements
		var parentPath = property.split('.');
		var child = parentPath.pop();
		var parent = helpers.getDeepObject(parentPath, obj);
		// turn this to array of methods to be traversed
		var methods = helpers.toArray(schema[property]);

		var hasProperty = parent && parent.hasOwnProperty(child);
		var condition = ~methods.indexOf('required') || hasProperty;
		if (options && options.nonStrict) {
			condition = hasProperty;
		}

		if (condition) {
			for (var i = 0; i < methods.length; i++) {
				var method = methods[i];
				method = helpers.getMethod(method);

				if (validator[method.method]) {
					var message;
					if (options.messages && options.messages[property] && options.messages[property][method.method]) {
						message = options.messages[property][method.method];
					}
					validator.check(parent[child], message);
					validator[method.method].apply(validator, method.args);
				}
			}
		}
	}

	return validator.getErrors();
};

// var schema = {
// 	propertyType: { 'isIn': [['house', 'apartment', 'townhouse', 'villa', 'terrace', 'building', 'other']] },
// 	rentType: { 'isIn': [['entire', 'part', 'private', 'shared']] },
// 	price: 'isFloat',
// 	address: [
// 		'notNull',
// 		'required'
// 	],
// 	suburb: [
// 		'notNull',
// 		'required'
// 	],
// 	bedrooms: 'isInt',
// 	bathrooms: 'isInt',
// 	carspaces: 'isInt',
// 	'lol.hey': 'isInt'
// };

// var obj = {
// 	propertyType: 'house',
// 	rentType: 'part',
// 	price: '89',
// 	address: 'Plitt St',
// 	suburb: 'Glebe',
// 	bedrooms: '1',
// 	bathrooms: '10',
// 	carspaces: '1',
// 	lol: {
// 		hey: '123'
// 	}
// };

// var err = exports.validate(obj, schema);
// console.log(err);