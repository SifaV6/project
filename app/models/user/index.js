/*
 * User Model
 */

/* Module dependencies */

var rootPath = process.cwd();
var appPath = rootPath + '/app/';

var db = require(appPath + 'mongo-db');
var ObjectId = require('mongojs').ObjectId;
var passHasher = require(rootPath + '/lib/pass-hasher');

/* schemas, filters and mappings */


/* Core functions for Users */

// Authenticate with email and password
// callback should be of form - callback(err, user, message)
exports.authenticate = function (email, password, callback) {
	db.users.findOne( { email: email }, function (err, user) {
		if (err) return callback(err);

		if (!user) {
			return callback(null, false, { message: 'Incorrect email' });
		}

		passHasher.compare(password, user.password, function (err, isMatch) {
			if (err) return callback(err);

			if (isMatch) {
				return callback(null, user);
			}

			return callback(null, false, { message: 'Incorrect password' });
		});
	});
};

// Register with email and password
// callback should be of form - callback(err, user, message)
exports.register = function (email, password, callback) {
	db.users.findOne({ email: email }, function (err, user) {
		if (err) return callback(err);

		// If user already exists, return user = false and a message
		if (user) {
			return callback(null, false, { message: 'Email already exists' });
		}

		passHasher.hash(password, function (err, hashedPassword) {
			if (err) return callback(err);

			db.users.insert({ email: email, password: hashedPassword }, function (err, user) {
				if (err) return callback(err);

				// callback with the user just inserted
				callback(null, user);
			});
		});
	});
};

// Register with Facebook