/*
 * Express config for all routes
 */

// app init
var express = require('express');
var app = module.exports = express();

/* Module dependencies */

var rootPath = process.cwd();

var express = require('express');
var passport = require('passport');

/*
 * all environments
 */

// middlewares
app.use(express.favicon());
app.use(express.compress());
app.use(express.logger('dev'));
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.session({ secret: 'keyboard cat' }));
app.use(passport.initialize());
app.use(passport.session());
// expose authenticated user to views
app.use(require(rootPath + '/lib/auth-user'));
app.use(express.static(rootPath + '/public'));
// attach livereload script to page if in dev mode
// added after static assets therefore doesn't get called for static assets
if ('development' == app.get('env')) {
  app.use(require('connect-livereload')({
    port: 35729
  }));
}
app.use(app.router);

/*
 * development mode
 */

if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}