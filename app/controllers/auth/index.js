/*
 * Login and logout
 */

// app init
var express = require('express');
var app = module.exports = express();
var appPath = process.cwd() + '/app/';

// view settings
app.set('views', appPath + 'views');
app.set('view engine', 'jade');

/* Module dependencies */

var passport = require('./passport-config');
var db = require(appPath + 'mongo-db');
var User = require(appPath + 'models/user');

/* Login routes */

// Local Strategy Login
// GET login form
app.get('/login', function (req, res) {
	if (req.isAuthenticated()) {
		return res.redirect('/');
	}

	res.render('auth/login', { messages: req.session.messages });

	req.session.messages = null;
});

// POST login details
app.post('/login', function (req, res, next) {
	passport.authenticate('local', function (err, user, message) {
		if (err) return next(err);
		if (!user) {
			console.log(message);
			req.session.messages = [message.message];
			return res.redirect('/login');
		}

		req.login(user, function (err) {
			if (err) return next(err);

			if (req.session.returnTo) {
				var url = req.session.returnTo;
				delete req.session.returnTo;

				return res.redirect(url);
			}
			else {
				return res.redirect('/');
			}
		});
	})(req, res, next);
});

// Facebook Login
// 
app.get('/login/facebook',
	passport.authenticate('facebook', { scope: ['email', 'user_about_me'] }));

app.get('/login/facebook/callback',
	passport.authenticate('facebook', { failureRedirect: '/login' }),
	function(req, res) {
		// Successful authentication, redirect home.
		res.redirect('/');
	}
);

// Logout
app.get('/logout', function (req, res) {
	req.logout();
	res.redirect('/');
});

// Register
// POST register details
app.post('/register', function (req, res, next) {
	var email = req.body.email;
	var password = req.body.password;

	User.register(email, password, function (err, user, message) {
		if (err) return next(err);

		if (!user) {
			req.session.messages = [message.message];
			return res.redirect('/login');
		}

		req.login(user, function (err) {
			if (err) return next(err);
			return res.redirect('/');
		});
	});
});