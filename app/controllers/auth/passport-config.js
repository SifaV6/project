/**
 * Module dependencies.
 */

var appPath = process.cwd() + '/app/';
var passport = module.exports = require('passport');
var db = require(appPath + 'mongo-db');
var User = require(appPath + 'models/user');

// strategies
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

// serialize and deserialize sessions
passport.serializeUser(function (user, done) {
	done(null, user.email); // using email to identify user, may switch to _id later
});

passport.deserializeUser(function (email, done) {
	db.users.findOne( { email: email }, function (err, user) {
		if (err) return done(err);
		if (!user) return done(new Error('failed to find user'));

		done(null, { _id: user._id, email: user.email });
	});
});

// Local Strategy with username and password
passport.use(new LocalStrategy({
		usernameField: 'email'
	},
	function (email, password, done) {
		User.authenticate(email, password, done);
	}
));

// Facebook Strategy
passport.use(new FacebookStrategy({
    clientID: '545735972151031',
    clientSecret: '0b85585c0a08054e9c70fc9ef84e9da4',
    callbackURL: 'http://localhost:3000/login/facebook/callback'
  },
  function(accessToken, refreshToken, profile, done) {
		db.users.findOne({ 'facebook.id': profile.id }, function (err, user) {
			if (err) return done(err);

			if (!user) {
				db.users.insert({
					email: profile.emails[0].value,
					facebook: profile._json
				}, function (err, user) {
					if (err) return done(err);

					return done(null, user);
				});
			} else {
				return done(null, user);
			}
		});
  }
));