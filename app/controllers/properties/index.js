/*
 * Properties
 */

// app init
var express = require('express');
var app = module.exports = express();
var appPath = process.cwd() + '/app/';

// view settings
app.set('views', appPath + 'views');
app.set('view engine', 'jade');

/* Module dependencies */

var rootPath = process.cwd();

var db = require(appPath + 'mongo-db');
var ObjectId = require('mongojs').ObjectId;
var get = require(appPath + 'controllers/properties-api/search').get;

/* Properties routes */

// middlewares
function ensureLogin(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	req.session.returnTo = req.originalUrl || req.url;
	res.redirect('/login');
}

// isOwner
function isOwner(req, res, next) {

}

// GET /properties/new
// get the form to list a new property
app.get('/properties/new', ensureLogin, function (req, res) {
	res.render('properties/new');
});

app.get('/properties/:id/edit', ensureLogin, function (req, res, next) {
	// Need to check if this guy is owner of this property or not
	get(req.params.id, function (err, property) {
		if (err) return next(err);

		if (property.ownerId + '' === req.user._id + '') {
			return res.render('properties/edit', { property: property });
		}
		else {
			return res.send('you\'re not the owner!!!');
		}
	});
	
});

// GET /properties/:id
// show a property by ID
app.get('/properties/:id', function (req, res, next) {
	db.properties.findOne({ _id: new ObjectId(req.params.id) }, function (err, property) {
		if (err) return next(err);

		res.render('properties/show', { property: property });
	});
});