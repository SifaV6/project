/*
 * home page.
 */

// app init
var express = require('express');
var app = module.exports = express();
var appPath = process.cwd() + '/app/';

// view settings
app.set('views', appPath + 'views');
app.set('view engine', 'jade');

// Get home page
app.get('/', function (req, res) {
	res.render('home/index');
});

// S3 signing
var policy = require('s3-policy');

app.get('/signS3', function (req, res) {
	var min = 60000;
	var hour = 60 * min;
	var now = Date.now();

	var p = policy({
		acl: 'public-read',
		expires: new Date(now + hour),
		bucket: 'buynlease',
		secret: '22McnjNSe5jZNwpIAt3jByMa2gXBjzXx0Zty2SLP',
		key: 'AKIAJRQHQ6T7G7ITFVBA'
		// length: // should add length restriction
	});

	var s3 = {
		policy: p.policy,
		signature: p.signature,
		bucket: 'buynlease',
		acl: 'public-read',
		key: 'AKIAJRQHQ6T7G7ITFVBA'
	};

	res.json(200, s3);
});

// Cloudinary signing
var cloudinary = require('cloudinary');
app.get('/signCloudinary', function (req, res) {

});