/* Module dependencies */

var rootPath = process.cwd();

var validator = require('revalidator');
var filter = require(rootPath + '/lib/mapper-filter').filter;
var map = require(rootPath + '/lib/mapper-filter').map;
var db = require(rootPath + '/app/mongo-db');
var ObjectId = require('mongojs').ObjectId;

/* Filter and Mapping */

var searchFilter = {
	suburb: 'toLowerCase',
	propertyType: 'toArray',
	rentType: 'toArray',
	bedrooms: 'toInt',
	'price.min': 'tofloat',
	'price.max': 'toFloat'
};

var queryMapping = {
	'suburb': 'suburb',
	'bedrooms': 'bedrooms',
	'price.min': 'price.$gte',
	'price.max': 'price.$lte',
	'propertyType': 'propertyType.$in',
	'rentType': 'rentType.$in'
};

/* Core functions */

exports.search = function (criteria, callback) {
	filter(criteria, searchFilter);
	console.log(criteria);
	var query = map(criteria, {}, queryMapping);
	console.log(query);
	db.properties.find(query, function (err, properties) {
		if (err) return callback(err);

		return callback(null, properties);
	});
};

exports.get = function (id, callback) {
	try {
		id = new ObjectId(id);
	} catch (e) {
		return callback(e);
	}

	db.properties.findOne({ _id: id }, function (err, property) {
		if (err) return callback(err);

		return callback(null, property);
	});
};