/* Module dependencies */

var rootPath = process.cwd();

var validate = require(rootPath + '/lib/schema-validator').validate;
var filter = require(rootPath + '/lib/mapper-filter').filter;
var db = require(rootPath + '/app/mongo-db');
var ObjectId = require('mongojs').ObjectId;

/* Schema */

var propertyTypes = [
	'house',
	'apartment',
	'townhouse',
	'villa',
	'terrace',
	'building',
	'other'
];

var rentTypes = [
	'entire',
	'part',
	'private',
	'shared'
];

var schema = {
	propertyType: { 'isIn': [propertyTypes] },
	rentType: { 'isIn': [rentTypes] },
	price: 'isFloat',
	address: [
		'notNull',
		'required'
	],
	suburb: [
		'notNull',
		'required'
	],
	bedrooms: 'isInt',
	bathrooms: 'isInt',
	carspaces: 'isInt'
};

var errMessages = {
	propertyType: {
		'isIn': 'Property type is not valid'
	},
	rentType: {
		'isIn': 'Rent type is not valid'
	},
	price: {
		'isFloat': 'Please enter a number for price'
	},
	address: {
		'notNull': 'Please enter a valid address'
	},
	suburb: {
		'notNull': 'Please enter a valid suburb'
	},
	bedrooms: {
		'isInt': 'Please enter a number for bedrooms'
	},
	bathrooms: {
		'isInt': 'Please enter a number for bathrooms'
	},
	carspaces: {
		'isInt': 'Please enter a number for carspaces'
	}
};

/* Filter and Mapping */

var listFilter = {
	suburb: 'toLowerCase',
	price: 'toFloat',
	bedrooms: 'toInt',
	bathrooms: 'toInt',
	carspaces: 'toInt'
};

/* Core functions */

exports.list = function (property, callback) {
	filter(property, listFilter);

	var errors = validate(property, schema, { messages: errMessages });
	if (errors) return callback(errors);

	db.properties.insert(property, function (err, property) {
		if (err) return callback(err);

		return callback(null, property);
	});
};

// Update the property
// This is very simple update that only works 1 level down the object tree
// Should modify to work with embedded documents as well
exports.update = function (id, property, callback) {
	filter(property, listFilter);

	var errors = validate(property, schema, { messages: errMessages, nonStrict: true });
	if (errors) return callback(errors);

	try {
		id = new ObjectId(id);
	} catch (e) {
		return callback(e);
	}

	db.properties.update({ _id: id }, { $set: property }, function (err) {
		if (err) return callback(err);

		return callback(null);
	});
};