/*
 * Properties API
 */

// app init
var express = require('express');
var app = module.exports = express();
var appPath = process.cwd() + '/app/';

/* Module dependencies */

var rootPath = process.cwd();

var db = require(appPath + 'mongo-db');
var search = require('./search').search;
var get = require('./search').get;
var list = require('./list').list;
var update = require('./list').update;

/* API routes */

// GET /properties
// Search for properties given criteria from query string
app.get('/api/properties', function (req, res, next) {
	console.log(req.query);
	search(req.query, function (err, properties) {
		if (err) return res.json(500, {status: 500, error: err});

		res.json(200, properties);
	});
});

// GET /properties/:id
// get a property given id
app.get('/api/properties/:id', function (req, res, next) {
	get(req.params.id, function (err, property) {
		if (err) return res.json(500, {status: 500, error: err});

		res.json(200, property);
	});
});

// POST /properties
// list a new property
app.post('/api/properties', function (req, res, next) {
	if (!req.user) return res.json(401, {status: 401, error: 'Unauthorized'});

	var property = req.body;
	property.ownerId = req.user._id;
	list(property, function (err, property) {
		if (err) return res.json(500, {status: 500, error: err});

		res.json(201, property);
	});
});

// PUT /properties/:id
// update a property given id
app.put('/api/properties/:id', function (req, res, next) {
	// Need to check if this guy is owner of this property or not!!!!!!
	update(req.params.id, req.body, function (err, property) {
		if (err) return res.json(500, {status: 500, error: err});

		res.json(200);
	});
});