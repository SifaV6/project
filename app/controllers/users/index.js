/*
 * Users
 */

// app init
var express = require('express');
var app = module.exports = express();
var appPath = process.cwd() + '/app/';

// view settings
app.set('views', appPath + 'views');
app.set('view engine', 'jade');

/* Module dependencies */

var rootPath = process.cwd();

var db = require(appPath + 'mongo-db');
var ObjectId = require('mongojs').ObjectId;

app.get('/users', function (req, res, next) {
	db.users.find({}, function (err, users) {
		if (err) return next(err);

		res.render('users/index', { users: users });
	});
});

// GET /users/:id
app.get('/users/:id', function (req, res, next) {
	db.users.findOne({ _id: new ObjectId(req.params.id) }, function (err, user) {
		if (err) return next(err);

		res.render('users/show', { user: user });
	});
});