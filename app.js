
/*
 * Module dependencies.
 */

var express = require('express');
var http = require('http');

var app = express();

app.use(require('./app/config'));

// routes

app.use(require('./app/controllers/home'));
app.use(require('./app/controllers/auth'));
app.use(require('./app/controllers/properties'));
app.use(require('./app/controllers/properties-api'));
app.use(require('./app/controllers/users'));

app.use(function (req, res, next) {
  res.send(404, 'Not found 404!!');
});

// Start the app by listening on <port>
app.set('port', process.env.PORT || 3000);
http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});